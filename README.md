# Pokemon Trainer App

## Authors
Sinan Selboe Karagülle
Felix Amundsen Zamura

## Functionality
This app makes you into a true pokemon trainer(kinda). You start by making your own name and logging into the
app. You can then buy pokemons for your collection in the catalogue. You can view your collection in the trainer page. The app saves your user inbetween sessions.

## Hosted on
https://immense-temple-13247.herokuapp.com/

https://powerful-bayou-74852.herokuapp.com/

## Setup guide
To setup the app you clone this repository and then in the terminal use the follwing commands:

npm install

followed by

ng serve

## Initial component page
![](./component_angular_initial.PNG)

## Final component page
![](./component_angular_final.PNG)


