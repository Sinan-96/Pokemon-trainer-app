import { Component, OnInit,Input } from '@angular/core';
import { Pokemon } from '../models/pokemon.model';
import { User } from '../models/user.model';
import { SessionService } from '../services/session.service';
import { UserService } from '../services/user.service';
import { Router,NavigationEnd  } from '@angular/router';


@Component({
  selector: 'app-pokemon-display',
  templateUrl: './pokemon-display.component.html',
  styleUrls: ['./pokemon-display.component.css']
})
export class PokemonDisplayComponent implements OnInit {

  @Input() pokemon:Pokemon | undefined;
  constructor(private readonly sessionService: SessionService,
    private router: Router) { }

  showRemove(){
    if(this.router.url === "/trainer"){
      return true;
    }
    else{
      return false;
    }
  }
  ngOnInit(): void {
  }

  getImageURL(){
  return `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${this.pokemon?.id}.png`
  }

  delete(){
    let userCopy = this.sessionService.user;
    let pokemons = userCopy?.pokemon;
    pokemons = pokemons?.filter(pokeName => pokeName !== this.pokemon?.name)
    const returnUser : User = {id : userCopy?.id ?? 0 , username:userCopy?.username ?? '', pokemon:pokemons ??[] }
    this.sessionService.setUser(returnUser);
  }

  add(){
    let userCopy = this.sessionService.user;
    let pokemons = userCopy?.pokemon;
    pokemons?.push(this.pokemon?.name ?? '');

    const returnUser : User = {id : userCopy?.id ?? 0 , username:userCopy?.username ?? '', pokemon:pokemons ??[] }
    this.sessionService.setUser(returnUser);

  }
}
