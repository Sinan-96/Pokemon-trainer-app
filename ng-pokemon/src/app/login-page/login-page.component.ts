import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from '../services/user.service'
import { SessionService } from "../services/session.service";
import { NgForm } from "@angular/forms";
import { PokemonService } from '../services/pokemon.service';
import {Pokemon} from "../models/pokemon.model";

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {

  constructor(
    private readonly router: Router,
    private readonly userService: UserService,
    private readonly pokemonService: PokemonService,
    private readonly sessionService: SessionService
    ) {
  }

  get attempting(): boolean {
    return this.userService.attempting;
  }
  addAllPokemons(){
    for(let pokemon of this.sessionService.user?.pokemon ?? []){
      this.pokemonService.getPokemonByName(pokemon);
    }
  }

  onSubmit(loginForm: NgForm) : void {
    console.log(loginForm.value["usernamInput"])
    this.userService.authenticate(loginForm.value['usernamInput'], async () => {
      this.addAllPokemons();
      this.pokemonService.getPokemon();
      await this.router.navigate(['trainer'])
    })

  }

  ngOnInit(): void {
    if (this.sessionService.user != undefined) {
      this.router.navigate(['trainer'])
    }
  }

}
