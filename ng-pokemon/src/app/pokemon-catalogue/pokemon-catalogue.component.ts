import {Component, OnDestroy, OnInit} from '@angular/core';
import { Pokemon } from '../models/pokemon.model';
import { PokemonService } from '../services/pokemon.service';
import { SessionService } from '../services/session.service';
import {UserService} from "../services/user.service";

@Component({
  selector: 'app-pokemon-catalogue',
  templateUrl: './pokemon-catalogue.component.html',
  styleUrls: ['./pokemon-catalogue.component.css']
})
export class PokemonCatalogueComponent implements OnInit, OnDestroy {

  constructor(
    private readonly pokemonService: PokemonService,
    private readonly sessionService : SessionService,
    private readonly userService: UserService
  ) { }

  get pokemons() : Pokemon[]{
    return this.sessionService.getPokemonsYouDontHave();
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    if(this.sessionService.user != undefined) {
      this.userService.updateUser(this.sessionService.user!);
    }
  }

}
