import {Component, OnInit, AfterViewInit, OnDestroy} from '@angular/core';
import { Router } from '@angular/router';
import { Pokemon } from '../models/pokemon.model';
import { User } from '../models/user.model';
import { PokemonService } from '../services/pokemon.service';
import { SessionService } from '../services/session.service';
import {UserService} from "../services/user.service";

@Component({
  selector: 'app-trainer',
  templateUrl: './trainer.component.html',
  styleUrls: ['./trainer.component.css']
})
export class TrainerComponent implements OnInit, OnDestroy {

  constructor(
    private readonly router: Router,
    private readonly pokemonService: PokemonService,
    private readonly sessionService: SessionService,
    private readonly userService: UserService
   ) { }

  ngOnInit(): void {
    if(!sessionStorage.getItem("gen1")){
      this.pokemonService.getPokemon();
    }
  }

  ngOnDestroy(): void {
    if(this.sessionService.user != undefined) {
      this.userService.updateUser(this.sessionService.user!);
    }
  }

  get pokemons() : Pokemon[]{
    return this.sessionService.getYourPokemons();
  }

  pokemonTrainerName():string{
    return this.sessionService.user?.username ?? '';
  }



}
