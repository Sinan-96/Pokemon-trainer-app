import { Component } from '@angular/core';
import { SessionService } from "./services/session.service";
import {Router} from "@angular/router";
import {PokemonService} from "./services/pokemon.service";
import {UserService} from "./services/user.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  constructor(
    private readonly router: Router,
    private readonly sessionService: SessionService,
    private readonly userService: UserService
  ) { }

  title = 'ng-pokemon';

  public async updateAndLogout() {
    if(this.sessionService.user != undefined) {
      await this.userService.updateUser(this.sessionService.user!)
    }
    this.sessionService.logout()
    this.router.navigate([''])
  }

}
