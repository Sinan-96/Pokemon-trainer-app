import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms'

import { AppComponent } from './app.component';
import { RouterModule, Routes } from '@angular/router';
import { LoginPageComponent } from './login-page/login-page.component';
import { TrainerComponent } from './trainer/trainer.component';
import { PokemonCatalogueComponent } from './pokemon-catalogue/pokemon-catalogue.component';
import { PokemonDisplayComponent } from './pokemon-display/pokemon-display.component'
import { AuthGuard } from "./services/auth.guard";
import {SessionService} from "./services/session.service";
import {UserService} from "./services/user.service";

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: '/login' },
  { path: 'login', component: LoginPageComponent },
  { path: 'trainer', component: TrainerComponent, canActivate: [AuthGuard] },
  { path: 'catalogue', component: PokemonCatalogueComponent, canActivate: [AuthGuard] }
]

@NgModule({
  declarations: [
    AppComponent,
    LoginPageComponent,
    TrainerComponent,
    PokemonCatalogueComponent,
    PokemonDisplayComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot(routes),
    HttpClientModule
  ],
  exports:[RouterModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {

}
