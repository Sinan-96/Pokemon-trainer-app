import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { config } from '../../environments/config.environment'
import { Observable } from 'rxjs';
import { User } from '../models/user.model'
import { of } from 'rxjs'
import { retry, finalize, switchMap, map, tap } from 'rxjs/operators'
import { Pokemon } from '../models/pokemon.model';
import { SessionService } from "./session.service";

const API_URL = 'https://pokeapi.co/api/v2/'

@Injectable({
    providedIn: 'root'
})

export class PokemonService {
    private pokemons: Pokemon[] = [];
    public attempting: boolean = false;
    public error: string = '';

    constructor(
      private readonly http: HttpClient,
      private readonly sessionService: SessionService) {
    }

    public getPokemon() {
        return this.http.get<{results: Pokemon[]}>(`${API_URL}pokemon/?limit=151`)
              .subscribe((response) => { // success
              console.log("AA")
              console.log(response.results)
              this.sessionService.setGen1(response.results)
                this.sessionService.setGen1(response.results)
            }, (error:HttpErrorResponse) => { // error
                this.error = error.message;
            })

    }
  public getPokemonByName(name: string){
    return this.http.get<Pokemon>(`${API_URL}pokemon/${name}`)
      .subscribe((pokemon:Pokemon) => {
        this.pokemons.push(pokemon)
      }, (error:HttpErrorResponse) => {
        this.error = error.message;
      })
  }

  public getPokemonByUrl(url: string){
    return this.http.get<Pokemon>(`${url}`)
      .subscribe((pokemon:Pokemon) => {
        this.pokemons.push(pokemon)
      }, (error:HttpErrorResponse) => {
        this.error = error.message;
      })
  }

  public getPokemons(): Pokemon[]{
        return this.pokemons;
    }

    public getError(): string {
        return this.error;
    }

}
