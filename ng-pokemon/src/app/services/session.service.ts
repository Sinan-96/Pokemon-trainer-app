import { Injectable } from "@angular/core";
import { User } from "../models/user.model"
import { Pokemon } from "../models/pokemon.model";

@Injectable({
  providedIn: 'root'
})

export class SessionService {

  private _gen1: Pokemon[] | undefined;
  private _user: User | undefined;

  constructor() {
    const storedUser = localStorage.getItem('user')
    if (storedUser) {
      this._user = JSON.parse(storedUser) as User;
    }
    const gen1 = sessionStorage.getItem('gen1')
    if(gen1){
      this._gen1 = JSON.parse(gen1) as Pokemon[];
    }
  }

  get user(): User | undefined {
    return this._user;
  }

  get gen1(): Pokemon[] | undefined {
    return this._gen1
  }

  setUser(user: User): void {
    this._user = user;
    localStorage.setItem('user', JSON.stringify(user));
  }

  setGen1(gen1: Pokemon[]): void {
    this._gen1 = gen1;
    console.log(gen1)
    sessionStorage.setItem('gen1', JSON.stringify(gen1))
  }
  getYourPokemons(){
    const yourPokemons:string[] = this.user?.pokemon ?? [];
    let pokemonList : Array<Pokemon> = []
    let Gen1 = this.gen1 ?? []
    for(let i = 0 ; i < Gen1.length ;i++){
      if(yourPokemons.indexOf(Gen1[i].name)> -1){
        const poke : Pokemon = {id : i +1 , name : Gen1[i].name };
        pokemonList.push(poke);
      }
    }
    return pokemonList;
  }

  getAllPokemons(){
    let pokemonList : Array<Pokemon> = []
    let Gen1 = this.gen1 ?? []
    for(let i = 0 ; i < Gen1.length ;i++){
      const poke : Pokemon = {id : i +1 , name : Gen1[i].name };
      pokemonList.push(poke);

    }
    return pokemonList;
  }

  getPokemonsYouDontHave(){
    const yourPokemons:string[] = this.user?.pokemon ?? [];
    let pokemonList : Array<Pokemon> = []
    let Gen1 = this.gen1 ?? []
    for(let i = 0 ; i < Gen1.length ;i++){
      if(!(yourPokemons.indexOf(Gen1[i].name)> -1)){
        const poke : Pokemon = {id : i +1 , name : Gen1[i].name };
        pokemonList.push(poke);
      }
    }
    return pokemonList;
  }

  logout() {

    this._user = undefined
    localStorage.removeItem('user')
  }
}
