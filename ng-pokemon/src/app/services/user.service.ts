import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { config } from '../../environments/config.environment'
import { Observable } from 'rxjs';
import { User } from '../models/user.model'
import { of } from 'rxjs'
import { retry, finalize, switchMap, tap } from 'rxjs/operators'
import { SessionService } from './session.service';

const API_URL = environment.apiBaseUrl
const API_KEY = config.apiKey

@Injectable({
    providedIn: 'root'
})

export class UserService {

    public attempting: boolean = false;
    public error: string = '';

    constructor(
        private readonly http: HttpClient,
        private readonly sessionService: SessionService) {

    }

    private findByUsername(username: string): Observable<User[]> {
        return this.http.get<User[]>(`${API_URL}/trainers?username=${username}`, {
            headers: new HttpHeaders({
                'X-API-Key': `${API_KEY}`,
                'Content-Type': 'application/json'
            })
        })
    }

    private createUser(username: string): Observable<User> {
        return this.http.post<User>(`${API_URL}/trainers`, { username }, {
            headers: new HttpHeaders({
                'X-API-Key': `${API_KEY}`,
                'Content-Type': 'application/json'
            })
        })
    }

  private putUser(user: User): Observable<User> {
    return this.http.put<User>(`${API_URL}/trainers/${user.id}`, {
      username: user.username,
      pokemon: user.pokemon }, {
      headers: new HttpHeaders({
        'X-API-Key': `${API_KEY}`,
        'Content-Type': 'application/json'
      })
    })
  }

  public updateUser(user: User) {
      this.putUser(user)
        .subscribe((user: User) => {
          console.log("successfully updated user")
        },
          (error: HttpErrorResponse) => {
          this.error = error.message
          })
  }

    public authenticate(username: string, onSuccess: () => void) {

        this.attempting = true;

        this.findByUsername(username)
        .pipe(
            retry(3),
            switchMap((users: User[]) => {
                if (users.length) {
                    console.log("user found: " + users[0].username)
                    return of(users[0])
                }
                console.log(users);
                return this.createUser(username)
            }),
            tap((user: User) => {

            }),
            finalize(() => {
                this.attempting = false;
            })
        )
        .subscribe(
            (user: User) => { // success
                if(user.id) {
                    this.sessionService.setUser(user);
                    onSuccess();
                }
            },
            (error: HttpErrorResponse) => { // error
                this.error = error.message;
            }
        )
    }
}
